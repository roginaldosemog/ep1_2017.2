# Conway's Game of Life
# Igor Aragão Gomes - 150011903
# Orientação a Objetos - Exercício de Programação 1

Como rodar o programa
1. Para ter esse projeto em seu computador, basta dar o comando "git clone https://gitlab.com/roginaldosemog/EP1", ou fazer o download do mesmo.
2. Dentro da pasta EP1, execute o comando "make", para compilar os códigos.
3. Utilize o comando "make run", para rodar o programa.

Como utilizar o programa
1. Com a intro do programa aberto, clique em qualquer tecla para continuar.
2. Em seguida, o programa apresentará o estado inicial de uma Gosper Glider Gun, e perguntar quantas gerações você deseja que o programa execute, a partir do estado inicial. Digite o valor de sua preferência.
3. Assista o decorrer das gerações. O programa encerrará sozinho.