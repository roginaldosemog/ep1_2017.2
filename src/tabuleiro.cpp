#include "tabuleiro.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Tabuleiro::Tabuleiro(){}

Tabuleiro::~Tabuleiro(){}

void Tabuleiro::setTabuleiroLimpo(){
  for(int i = 0; i < 40; i++)
    for(int j = 0; j < 100; j++)
      this->matriz[i][j] = ' ';
}

void Tabuleiro::setTabuleiroGliderGun(){
  ifstream glider ("doc/glider.txt");

	string aux;

	for(int i = 0; i < 40; i++){
		getline(glider, aux);
		for(int j = 0; j < 100; j++){
			this->matriz[i][j] = aux[j];
		}
	}

	glider.close();
}

void Tabuleiro::getTabuleiro(){
  for(int i = 0; i < 40; i++){
		for(int j = 0; j < 100; j++){
		 	cout << this->matriz[i][j];
		 	if(j >= 99)
		 		cout << endl;
		}
	}
}

void Tabuleiro::atualizaTabuleiro(){
  char matrizAux[40][100];
  int vizinhos;
  for(int i = 0; i < 40; i++){
    for(int j = 0; j < 100; j++){
      vizinhos=0;
      if(this->matriz[i-1][j-1]=='*')
        vizinhos++;
      if(this->matriz[i][j-1] == '*')
        vizinhos++;
      if(this->matriz[i+1][j-1]=='*')
        vizinhos++;
      if(this->matriz[i-1][j] == '*')
        vizinhos++;
      if(this->matriz[i+1][j] == '*')
        vizinhos++;
      if(this->matriz[i-1][j+1]=='*')
        vizinhos++;
      if(this->matriz[i][j+1] == '*')
        vizinhos++;
      if(this->matriz[i+1][j+1]=='*')
        vizinhos++;

      if(this->matriz[i][j]=='*'){
        if(vizinhos==2 || vizinhos==3)
          matrizAux[i][j] = '*';
        else
          matrizAux[i][j] = ' ';
      }
      else if(this->matriz[i][j]==' ') {
        if(vizinhos==3)
          matrizAux[i][j] = '*';
        else
          matrizAux[i][j] = ' ';
      } else {
        matrizAux[i][j] = ' ';
      }
    }
  }

  for(int i = 0; i < 40; i++)
    for(int j = 0; j < 100; j++)
      this->matriz[i][j] = matrizAux[i][j];
}
