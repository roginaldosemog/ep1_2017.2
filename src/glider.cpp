#include "forma.hpp"
#include "glider.hpp"
#include <iostream>

Glider::Glider(){};

Glider::Glider(int posX, int posY){
  this->setPosX(posX);
  this->setPosY(posY);
  this->setLarguraX(1);
  this->setLarguraY(3);
}

Glider::~Glider(){}

void Glider::setDesenho(){
  this->desenho[0][0] = ' ';
  this->desenho[0][1] = '*';
  this->desenho[0][2] = ' ';
  this->desenho[1][0] = ' ';
  this->desenho[1][1] = ' ';
  this->desenho[1][2] = '*';
  this->desenho[2][0] = '*';
  this->desenho[2][1] = '*';
  this->desenho[2][2] = '*';
}
