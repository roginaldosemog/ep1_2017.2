#include <iostream>
#include "forma.hpp"

using namespace std;

Forma::Forma(){}

Forma::~Forma(){}

void Forma::setPosX(int value){
  this->posX = value;
}

int Forma::getPosX(){
  return this->posX;
}

void Forma::setPosY(int value){
  this->posY = value;
}

int Forma::getPosY(){
  return this->posY;
}

void Forma::setLarguraX(int value){
  this->larguraX = value;
}

int Forma::getLarguraX(){
  return this->larguraX;
}

void Forma::setLarguraY(int value){
  this->larguraY = value;
}

int Forma::getLarguraY(){
  return this->larguraY;
}
