#include "forma.hpp"
#include "block.hpp"
#include <iostream>

Block::Block(){};

Block::Block(int posX, int posY){
  this->setPosX(posX);
  this->setPosY(posY);
  this->setLarguraX(2);
  this->setLarguraY(2);
}

Block::~Block(){}

void Block::setDesenho(){
  this->desenho[0][0] = '*';
  this->desenho[0][1] = '*';
  this->desenho[1][0] = '*';
  this->desenho[1][1] = '*';
}
