#include "tabuleiro.hpp"
#include "forma.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "menu.hpp"
#include <iostream>
#include <unistd.h>
#include <cstdlib>

using namespace std;

int main() {
  Menu * menu = new Menu();
  Tabuleiro * tabuleiro = new Tabuleiro();

  system("clear");
  menu->printMenu();

/*
  int modo;
  cout << "1) MODO GOSPER GLIDER GUN" << endl;
  cout << "2) MODO MONTAGEM";
  cout << "Indique o modo desejado: ";
  cin >> modo;
  while(modo != 1 || modo != 2){
    cout << "Digite uma opção válida!" << endl;
    cout << "Indique o modo desejado: ";
    cin >> modo;
  }

  if(modo==1){
    tabuleiro->setTabuleiroGliderGun();
  } else {
    tabuleiro->montaTabuleiro();
  }
*/

  system("clear");
  tabuleiro->setTabuleiroGliderGun(); //////

  tabuleiro->getTabuleiro();

  int limite, geracao=0;
  cout << "Insira a quantidade de gerações desejadas: ";
  cin >> limite;

  while (true) {
    system("clear");
    tabuleiro->getTabuleiro();
    tabuleiro->atualizaTabuleiro();

    cout << "Geração: " << geracao << endl;

    usleep(75000);

    if(geracao >= limite)
      break;
    geracao++;
  }

  delete(menu);
  delete(tabuleiro);

  return 0;
}
