#include "forma.hpp"
#include "blinker.hpp"
#include <iostream>

Blinker::Blinker(){};

Blinker::Blinker(int posX, int posY){
  this->setPosX(posX);
  this->setPosY(posY);
  this->setLarguraX(1);
  this->setLarguraY(3);
}

Blinker::~Blinker(){}

void Blinker::setDesenho(){
  this->desenho[0][0] = '*';
  this->desenho[0][1] = '*';
  this->desenho[0][2] = '*';
}
