#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "forma.hpp"

class Block : public Forma{
private:
  char desenho[2][2];
public:
  Block();
  Block(int posX, int posY);
  ~Block();

  void setDesenho();
};

#endif
