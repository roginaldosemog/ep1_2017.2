#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "forma.hpp"

class Blinker : public Forma{
private:
  char desenho[2][2];
public:
  Blinker();
  Blinker(int posX, int posY);
  ~Blinker();

  void setDesenho();
};

#endif
