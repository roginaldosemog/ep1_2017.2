#ifndef TABULEIRO_HPP
#define TABULEIRO_HPP

#include <iostream>
#include "forma.hpp"
#include "block.hpp"

class Tabuleiro{
private:
  char matriz[40][100];

public:
  Tabuleiro();
  ~Tabuleiro();

  void setTabuleiroLimpo();
  void setTabuleiroGliderGun();

  void getTabuleiro();
  void atualizaTabuleiro();
};

#endif
