#ifndef GLIDER_HPP
#define GLIDER_HPP

#include "forma.hpp"

class Glider : public Forma{
private:
  char desenho[3][3];
public:
  Glider();
  Glider(int posX, int posY);
  ~Glider();

  void setDesenho();
};

#endif
