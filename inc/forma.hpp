#ifndef FORMA_HPP
#define FORMA_HPP

#include <iostream>

class Forma{
private:
  int posX;
  int posY;
  int larguraX;
  int larguraY;

public:
  Forma();
  virtual ~Forma()=0;

  void setPosX(int posX);
  int getPosX();
  void setPosY(int posY);
  int getPosY();

  void setLarguraX(int larguraX);
  int getLarguraX();
  void setLarguraY(int larguraY);
  int getLarguraY();
};

#endif
